package connectmodel;

import java.awt.Point;
import java.util.Vector;

public class GameBoard
{
    private int myNumRows;
    private int myNumColumns;
    private PieceType[][] myBoard; //don't know how this works
    private int myNumTypes;
    private int myWinLength;
    private Point myLastPoint;
    private Vector<PieceType> myTypes;
    private Point myWinBegin;
    private Point myWinEnd;
    private boolean myIsAWin;
	
    public GameBoard(int rows, int cols, int winLength, PieceType[] types) 
    {
    	
    }
	
    public boolean placePiece(int col, PieceType type)
    {
    	return false;
    }
	
    public void resetBoard() 
    {
    }
	
    public boolean checkIfWin() 
    {
	    return false;
    }
	
    public int findBestMoveColumn(PieceType type)
    {
	    return 0;
    }
	
    private boolean checkVerticalWin() 
    {
	    return false;
    }
	
    private boolean checkHorizontalWin() 
    {
	    return false;
    }
	
    private boolean checkDiagonalWin() 
    {
	    return false;
    }
	
    private int countHorizontalLengthIfPiecePlaced(int col, PieceType type) 
    {
	    return 0;
    }
	
    private int countVerticalLengthIfPiecePlaced(int col, PieceType type) 
    {
	    return 0;
    }
	
    private int countDiagonalLengthIfPiecePlaced(int col, PieceType type) 
    {
	    return 0;
    }
	
    public Vector<PieceType> getTypes() 
    {
	    return null;
    }
	
    public Point getWinBegin() 
    {
	    return null;
    }
	
    public Point getWinEnd()
    {
	    return null;
    }
	
    public Point getLastPoint() 
    {
	    return null;
    }
	
    public PieceType getPieceOnBoard(Point point) 
    {
	    return null;
    }
	
    public PieceType[][] getBoard() 
    {
	    return null;
    }
	
    public boolean isBoardFull() 
    {
	    return false;
    }
	
    public boolean isColumnFull(int col) 
    {
	    return false;
    }
	
    public boolean getIsAWin() 
    {
	    return false;
    }
    
    public boolean checkAllNull()
    {
		return false;
    }
}