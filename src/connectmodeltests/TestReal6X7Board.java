package connectmodeltests;

/**
 * @purpose Test6x7Board tests the Connect Four Model
 * with the conditions of a 6x7 board.
 * @author williamcordarorios
 *
 * @datedue February 27, 2019, 11:59pm
 */


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import java.awt.Point;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import connectmodel.GameBoard;
import connectmodel.PieceType;

class TestReal6X7Board
{
	private GameBoard myGameBoard;
	private PieceType[] myPieceTypes;

	@BeforeEach
	void setUp() throws Exception 
	{
		myPieceTypes = new PieceType[2];
		myPieceTypes[0] = PieceType.RED;
		myPieceTypes[1] = PieceType.BLACK;
		myGameBoard = new GameBoard(6, 7, 4, myPieceTypes);
	}
	
	@Test
	void testPlacePiece()
	{
		myGameBoard.placePiece(0, PieceType.RED);
		Point last = myGameBoard.getLastPoint();
		int x = last.x;
		assertTrue("Piece was placed", x == 0);
	}

	@Test
	void testPieceOutOfBounds() 
	{
		myGameBoard.placePiece(-5, PieceType.RED);
		Point last = myGameBoard.getLastPoint();
		int x = last.x;
		assertFalse("Piece shouldn't be on board", x == -5);
		myGameBoard.placePiece(9, PieceType.RED);
		Point last2 = myGameBoard.getLastPoint();
		int x2 = last2.x;
		assertFalse("Piece shouldn't be on board", x2 == 9);
	}
	
	@Test
	void testOverlappingPieces() 
	{
		myGameBoard.placePiece(0, PieceType.RED);
		myGameBoard.placePiece(0, PieceType.RED);
		Point last = myGameBoard.getLastPoint();
		int x = last.x;
		int y = last.y;
		assertTrue("Piece should be in column 0", x == 0);
		assertTrue("Piece should be in row 2", y == 1);
		assertFalse("Piece is in the incorrect row", y == 0);
	}
	
	@Test
	void testHorizontalLengthCount ()
	{
		myGameBoard.placePiece(0, PieceType.BLACK);
		myGameBoard.placePiece(1, PieceType.RED);
		myGameBoard.placePiece(2, PieceType.RED);
		Point last = myGameBoard.getLastPoint();
		int x = last.x;
		assertTrue("Horizontal length of last piece type is 2", x == 2);
		myGameBoard.placePiece(3, PieceType.RED);
		Point last2 = myGameBoard.getLastPoint();
		int y = last2.y;
		assertTrue("Horizontal length of last piece type is 3", y == 3);
	}
	
	@Test
	void testVerticalLengthCount ()
	{
		myGameBoard.placePiece(0, PieceType.RED);
		myGameBoard.placePiece(0, PieceType.BLACK);
		myGameBoard.placePiece(0, PieceType.BLACK);
		Point last = myGameBoard.getLastPoint();
		int x = last.x;
		assertTrue("Vertical length of last piece type is 2", x == 0);
		myGameBoard.placePiece(0, PieceType.BLACK);
		Point last2 = myGameBoard.getLastPoint();
		int y = last2.y;
		assertTrue("Vertical length of last piece type is 3", y == 0);
	}
	
	@Test
	void testDiagonalLengthCount ()
	{
		myGameBoard.placePiece(1, PieceType.RED);
		myGameBoard.placePiece(2, PieceType.RED);
		myGameBoard.placePiece(2, PieceType.RED);
		myGameBoard.placePiece(0, PieceType.BLACK);
		myGameBoard.placePiece(1, PieceType.BLACK);
		Point last = myGameBoard.getLastPoint();
		int x = last.x;
		assertTrue("Diagonal length of last piece type is 2", x == 1);
		myGameBoard.placePiece(2, PieceType.BLACK);
		Point last2 = myGameBoard.getLastPoint();
		int y = last2.y;
		assertTrue("Diagonal length of last piece type is 3", y == 2);
	}
	
	
	@Test
	void testRowIndexCorrect() 
	{
		myGameBoard.placePiece(0, PieceType.RED);
		Point last = myGameBoard.getLastPoint();
		int x = last.x;
		int y = last.y;
		assertTrue("Piece should be in column 0", x == 0);
		assertTrue("Piece should be in row 0", y == 0);
		assertFalse("Rows seem inverted", y == 5);
	}
	
	//TODO
	@Test
	void testBestMoveColumn()
	{
		myGameBoard.placePiece(1, PieceType.BLACK);
		myGameBoard.placePiece(2, PieceType.BLACK);
		myGameBoard.placePiece(3, PieceType.BLACK);
		assertFalse("Game should not be won yet", myGameBoard.checkIfWin());
		myGameBoard.findBestMoveColumn(PieceType.BLACK);
		assertTrue("Game should have been won ", myGameBoard.checkIfWin());
		
		//For GameBoard, since there are many ways to pick the best next move in findBestMoveColumn(), 
		//it should only test the following:  if the next turn for placing a piece should lead to a win, 
		//it should pick that first.  If not a win, but placing a piece should block the other player, 
		//it should pick that move.  Otherwise, placing a piece should count the total number of horizontal, 
		//vertical and diagonal pieces up to "myWinLength" away from the piece to be placed and pick the maximum. 
	}
	
	@Test
	void testDiagonalWinBottomLeftCorner()
	{
		myGameBoard.placePiece(1, PieceType.BLACK);
		myGameBoard.placePiece(2, PieceType.BLACK);
		myGameBoard.placePiece(2, PieceType.BLACK);
		myGameBoard.placePiece(3, PieceType.BLACK);
		assertFalse("Game should not have been won yet", myGameBoard.checkIfWin());
		myGameBoard.placePiece(3, PieceType.BLACK);
		myGameBoard.placePiece(3, PieceType.BLACK);
		myGameBoard.placePiece(0, PieceType.RED);
		myGameBoard.placePiece(1, PieceType.RED);
		myGameBoard.placePiece(2, PieceType.RED);
		myGameBoard.placePiece(3, PieceType.RED);
		assertTrue("Game should have been won ", myGameBoard.checkIfWin());
		Point begin = myGameBoard.getWinBegin();
		Point end = myGameBoard.getWinEnd();
	}
	
	@Test
	void testVerticalWinBottomLeftCorner()
	{
		myGameBoard.placePiece(0, PieceType.RED);
		myGameBoard.placePiece(0, PieceType.RED);
		myGameBoard.placePiece(0, PieceType.RED);
		myGameBoard.placePiece(3, PieceType.BLACK);
		myGameBoard.placePiece(3, PieceType.BLACK);
		myGameBoard.placePiece(1, PieceType.BLACK);
		myGameBoard.placePiece(2, PieceType.BLACK);
		assertFalse("Game should not have been won yet", myGameBoard.checkIfWin());
		myGameBoard.placePiece(0, PieceType.RED);
		assertTrue("Game should have been won ", myGameBoard.checkIfWin());
		Point begin = myGameBoard.getWinBegin();
		Point end = myGameBoard.getWinEnd();
	}
	
	@Test
	void testHorizontalWinBottomLeftCorner()
	{
		myGameBoard.placePiece(0, PieceType.RED);
		myGameBoard.placePiece(1, PieceType.RED);
		myGameBoard.placePiece(2, PieceType.RED);
		myGameBoard.placePiece(0, PieceType.BLACK);
		myGameBoard.placePiece(0, PieceType.BLACK);
		myGameBoard.placePiece(1, PieceType.BLACK);
		myGameBoard.placePiece(2, PieceType.BLACK);
		assertFalse("Game should not have been won yet", myGameBoard.checkIfWin());
		myGameBoard.placePiece(3, PieceType.RED);
		assertTrue("Game should have been won ", myGameBoard.checkIfWin());
		Point begin = myGameBoard.getWinBegin();
		Point end = myGameBoard.getWinEnd();
	}
	
	
	@Test
	void testDiagonalWinBottomRightCorner()
	{
		myGameBoard.placePiece(5, PieceType.RED);
		myGameBoard.placePiece(4, PieceType.RED);
		myGameBoard.placePiece(4, PieceType.RED);
		myGameBoard.placePiece(3, PieceType.RED);
		assertFalse("Game should not have been won yet", myGameBoard.checkIfWin());
		myGameBoard.placePiece(3, PieceType.RED);
		myGameBoard.placePiece(3, PieceType.RED);
		myGameBoard.placePiece(6, PieceType.BLACK);
		myGameBoard.placePiece(5, PieceType.BLACK);
		myGameBoard.placePiece(4, PieceType.BLACK);
		myGameBoard.placePiece(3, PieceType.BLACK);
		assertTrue("Game should have been won ", myGameBoard.checkIfWin());
		Point begin = myGameBoard.getWinBegin();
		Point end = myGameBoard.getWinEnd();
	}

	@Test
	void testVerticalWinBottomRightCorner()
	{
		myGameBoard.placePiece(6, PieceType.BLACK);
		myGameBoard.placePiece(6, PieceType.BLACK);
		myGameBoard.placePiece(6, PieceType.BLACK);
		myGameBoard.placePiece(3, PieceType.RED);
		myGameBoard.placePiece(3, PieceType.RED);
		myGameBoard.placePiece(1, PieceType.RED);
		myGameBoard.placePiece(2, PieceType.RED);
		assertFalse("Game should not have been won yet", myGameBoard.checkIfWin());
		myGameBoard.placePiece(6, PieceType.BLACK);
		assertTrue("Game should have been won ", myGameBoard.checkIfWin());
		Point begin = myGameBoard.getWinBegin();
		Point end = myGameBoard.getWinEnd();
	}
	
	@Test
	void testHorizontalWinBottomRightCorner()
	{
		myGameBoard.placePiece(6, PieceType.BLACK);
		myGameBoard.placePiece(5, PieceType.BLACK);
		myGameBoard.placePiece(4, PieceType.BLACK);
		myGameBoard.placePiece(6, PieceType.RED);
		myGameBoard.placePiece(6, PieceType.RED);
		myGameBoard.placePiece(5, PieceType.RED);
		myGameBoard.placePiece(4, PieceType.RED);
		assertFalse("Game should not have been won yet", myGameBoard.checkIfWin());
		myGameBoard.placePiece(3, PieceType.BLACK);
		assertTrue("Game should have been won ", myGameBoard.checkIfWin());
		Point begin = myGameBoard.getWinBegin();
		Point end = myGameBoard.getWinEnd();
	}
	
	@Test
	void testDiagonalWinTopRightCorner()
	{
		myGameBoard.placePiece(6, PieceType.RED);
		myGameBoard.placePiece(6, PieceType.RED);
		myGameBoard.placePiece(6, PieceType.RED);
		myGameBoard.placePiece(6, PieceType.BLACK);
		myGameBoard.placePiece(6, PieceType.BLACK);
		myGameBoard.placePiece(6, PieceType.BLACK);
		myGameBoard.placePiece(5, PieceType.RED);
		myGameBoard.placePiece(5, PieceType.RED);
		myGameBoard.placePiece(5, PieceType.BLACK);
		assertFalse("Game should not have been won yet", myGameBoard.checkIfWin());
		myGameBoard.placePiece(5, PieceType.RED);
		myGameBoard.placePiece(5, PieceType.BLACK);
		myGameBoard.placePiece(4, PieceType.RED);
		myGameBoard.placePiece(4, PieceType.RED);
		myGameBoard.placePiece(4, PieceType.BLACK);
		myGameBoard.placePiece(4, PieceType.BLACK);
		myGameBoard.placePiece(3, PieceType.BLACK);
		myGameBoard.placePiece(3, PieceType.BLACK);
		myGameBoard.placePiece(3, PieceType.BLACK);
		assertTrue("Game should have been won ", myGameBoard.checkIfWin());
		Point begin = myGameBoard.getWinBegin();
		Point end = myGameBoard.getWinEnd();
	}

	@Test
	void testVerticalWinTopRightCorner()
	{
		myGameBoard.placePiece(6, PieceType.RED);
		myGameBoard.placePiece(6, PieceType.RED);
		myGameBoard.placePiece(6, PieceType.BLACK);
		myGameBoard.placePiece(6, PieceType.BLACK);
		myGameBoard.placePiece(5, PieceType.RED);
		myGameBoard.placePiece(4, PieceType.RED);
		assertFalse("Game should not have been won yet", myGameBoard.checkIfWin());
		myGameBoard.placePiece(6, PieceType.BLACK);
		myGameBoard.placePiece(6, PieceType.BLACK);
		assertTrue("Game should have been won ", myGameBoard.checkIfWin());
		Point begin = myGameBoard.getWinBegin();
		Point end = myGameBoard.getWinEnd();
	}

	@Test
	void testHorizontalWinTopRightCorner()
	{
		myGameBoard.placePiece(6, PieceType.RED);
		myGameBoard.placePiece(6, PieceType.RED);
		myGameBoard.placePiece(6, PieceType.RED);
		myGameBoard.placePiece(6, PieceType.BLACK);
		myGameBoard.placePiece(6, PieceType.BLACK);
		myGameBoard.placePiece(6, PieceType.BLACK);
		myGameBoard.placePiece(5, PieceType.BLACK);
		assertFalse("Game should not have been won yet", myGameBoard.checkIfWin());
		myGameBoard.placePiece(5, PieceType.BLACK);
		myGameBoard.placePiece(5, PieceType.BLACK);
		myGameBoard.placePiece(5, PieceType.RED);
		myGameBoard.placePiece(5, PieceType.RED);
		myGameBoard.placePiece(5, PieceType.BLACK);
		myGameBoard.placePiece(4, PieceType.RED);
		myGameBoard.placePiece(4, PieceType.RED);
		myGameBoard.placePiece(4, PieceType.RED);
		myGameBoard.placePiece(4, PieceType.BLACK);
		myGameBoard.placePiece(4, PieceType.BLACK);
		myGameBoard.placePiece(3, PieceType.BLACK);
		myGameBoard.placePiece(3, PieceType.BLACK);
		myGameBoard.placePiece(3, PieceType.BLACK);
		myGameBoard.placePiece(3, PieceType.RED);
		myGameBoard.placePiece(3, PieceType.RED);
		myGameBoard.placePiece(3, PieceType.BLACK);
		myGameBoard.placePiece(4, PieceType.BLACK);
		assertTrue("Game should have been won ", myGameBoard.checkIfWin());
		Point begin = myGameBoard.getWinBegin();
		Point end = myGameBoard.getWinEnd();
	}

	@Test
	void testDiagonalWinTopLeftCorner()
	{
		myGameBoard.placePiece(0, PieceType.BLACK);
		myGameBoard.placePiece(0, PieceType.BLACK);
		myGameBoard.placePiece(0, PieceType.BLACK);
		myGameBoard.placePiece(0, PieceType.RED);
		myGameBoard.placePiece(0, PieceType.RED);
		myGameBoard.placePiece(0, PieceType.RED);
		myGameBoard.placePiece(1, PieceType.BLACK);
		myGameBoard.placePiece(1, PieceType.BLACK);
		myGameBoard.placePiece(1, PieceType.RED);
		assertFalse("Game should not have been won yet", myGameBoard.checkIfWin());
		myGameBoard.placePiece(1, PieceType.BLACK);
		myGameBoard.placePiece(1, PieceType.RED);
		myGameBoard.placePiece(2, PieceType.BLACK);
		myGameBoard.placePiece(2, PieceType.BLACK);
		myGameBoard.placePiece(2, PieceType.RED);
		myGameBoard.placePiece(2, PieceType.RED);
		myGameBoard.placePiece(3, PieceType.RED);
		myGameBoard.placePiece(3, PieceType.RED);
		myGameBoard.placePiece(3, PieceType.RED);
		assertTrue("Game should have been won ", myGameBoard.checkIfWin());
		Point begin = myGameBoard.getWinBegin();
		Point end = myGameBoard.getWinEnd();
	}

	@Test
	void testVerticalWinTopLeftCorner()
	{
		myGameBoard.placePiece(0, PieceType.BLACK);
		myGameBoard.placePiece(0, PieceType.BLACK);
		myGameBoard.placePiece(0, PieceType.RED);
		myGameBoard.placePiece(0, PieceType.RED);
		myGameBoard.placePiece(1, PieceType.BLACK);
		myGameBoard.placePiece(2, PieceType.BLACK);
		assertFalse("Game should not have been won yet", myGameBoard.checkIfWin());
		myGameBoard.placePiece(0, PieceType.RED);
		myGameBoard.placePiece(0, PieceType.RED);
		assertTrue("Game should have been won ", myGameBoard.checkIfWin());
		Point begin = myGameBoard.getWinBegin();
		Point end = myGameBoard.getWinEnd();
	}

	@Test
	void testHorizontalWinTopLeftCorner()
	{
		myGameBoard.placePiece(0, PieceType.BLACK);
		myGameBoard.placePiece(0, PieceType.BLACK);
		myGameBoard.placePiece(0, PieceType.BLACK);
		myGameBoard.placePiece(0, PieceType.RED);
		myGameBoard.placePiece(0, PieceType.RED);
		myGameBoard.placePiece(0, PieceType.RED);
		myGameBoard.placePiece(1, PieceType.RED);
		assertFalse("Game should not have been won yet", myGameBoard.checkIfWin());
		myGameBoard.placePiece(1, PieceType.RED);
		myGameBoard.placePiece(1, PieceType.RED);
		myGameBoard.placePiece(1, PieceType.BLACK);
		myGameBoard.placePiece(1, PieceType.BLACK);
		myGameBoard.placePiece(1, PieceType.RED);
		myGameBoard.placePiece(2, PieceType.BLACK);
		myGameBoard.placePiece(2, PieceType.BLACK);
		myGameBoard.placePiece(2, PieceType.BLACK);
		myGameBoard.placePiece(2, PieceType.RED);
		myGameBoard.placePiece(2, PieceType.RED);
		myGameBoard.placePiece(3, PieceType.RED);
		myGameBoard.placePiece(3, PieceType.RED);
		myGameBoard.placePiece(3, PieceType.RED);
		myGameBoard.placePiece(3, PieceType.BLACK);
		myGameBoard.placePiece(3, PieceType.BLACK);
		myGameBoard.placePiece(3, PieceType.RED);
		myGameBoard.placePiece(2, PieceType.RED);
		assertTrue("Game should have been won ", myGameBoard.checkIfWin());
		Point begin = myGameBoard.getWinBegin();
		Point end = myGameBoard.getWinEnd();
	}

	@Test
	void testBottomMiddleHorizontalWin()
	{
		myGameBoard.placePiece(3, PieceType.RED);
		myGameBoard.placePiece(1, PieceType.RED);
		myGameBoard.placePiece(2, PieceType.RED);
		myGameBoard.placePiece(0, PieceType.BLACK);
		myGameBoard.placePiece(0, PieceType.BLACK);
		myGameBoard.placePiece(1, PieceType.BLACK);
		myGameBoard.placePiece(2, PieceType.BLACK);
		assertFalse("Game should not have been won yet", myGameBoard.checkIfWin());
		myGameBoard.placePiece(4, PieceType.RED);
		assertTrue("Game should have been won ", myGameBoard.checkIfWin());
		Point begin = myGameBoard.getWinBegin();
		Point end = myGameBoard.getWinEnd();
	}

	@Test
	void testTopMiddleHorizontalWin()
	{
		myGameBoard.placePiece(2, PieceType.BLACK);
		myGameBoard.placePiece(2, PieceType.BLACK);
		myGameBoard.placePiece(2, PieceType.BLACK);
		myGameBoard.placePiece(2, PieceType.RED);
		myGameBoard.placePiece(2, PieceType.RED);
		myGameBoard.placePiece(2, PieceType.RED);
		myGameBoard.placePiece(3, PieceType.RED);
		assertFalse("Game should not have been won yet", myGameBoard.checkIfWin());
		myGameBoard.placePiece(3, PieceType.RED);
		myGameBoard.placePiece(3, PieceType.RED);
		myGameBoard.placePiece(3, PieceType.BLACK);
		myGameBoard.placePiece(3, PieceType.BLACK);
		myGameBoard.placePiece(3, PieceType.RED);
		myGameBoard.placePiece(4, PieceType.BLACK);
		myGameBoard.placePiece(4, PieceType.BLACK);
		myGameBoard.placePiece(4, PieceType.BLACK);
		myGameBoard.placePiece(4, PieceType.RED);
		myGameBoard.placePiece(4, PieceType.RED);
		myGameBoard.placePiece(5, PieceType.RED);
		myGameBoard.placePiece(5, PieceType.RED);
		myGameBoard.placePiece(5, PieceType.RED);
		myGameBoard.placePiece(5, PieceType.BLACK);
		myGameBoard.placePiece(5, PieceType.BLACK);
		myGameBoard.placePiece(5, PieceType.RED);
		myGameBoard.placePiece(4, PieceType.RED);
		assertTrue("Game should have been won ", myGameBoard.checkIfWin());
		Point begin = myGameBoard.getWinBegin();
		Point end = myGameBoard.getWinEnd();
	}
	
	@Test
	void testBottomMiddleVerticalWin()
	{
		myGameBoard.placePiece(3, PieceType.RED);
		myGameBoard.placePiece(3, PieceType.RED);
		myGameBoard.placePiece(4, PieceType.BLACK);
		myGameBoard.placePiece(4, PieceType.BLACK);
		myGameBoard.placePiece(2, PieceType.BLACK);
		myGameBoard.placePiece(2, PieceType.BLACK);
		myGameBoard.placePiece(2, PieceType.BLACK);
		assertFalse("Game should not have been won yet", myGameBoard.checkIfWin());
		myGameBoard.placePiece(3, PieceType.RED);
		myGameBoard.placePiece(3, PieceType.RED);
		assertTrue("Game should have been won ", myGameBoard.checkIfWin());
		Point begin = myGameBoard.getWinBegin();
		Point end = myGameBoard.getWinEnd();
	}
	
	@Test
	void testTopMiddleVerticalWin()
	{
		myGameBoard.placePiece(3, PieceType.BLACK);
		myGameBoard.placePiece(3, PieceType.BLACK);
		myGameBoard.placePiece(4, PieceType.BLACK);
		myGameBoard.placePiece(2, PieceType.BLACK);
		myGameBoard.placePiece(3, PieceType.RED);
		myGameBoard.placePiece(3, PieceType.RED);
		myGameBoard.placePiece(3, PieceType.RED);
		assertFalse("Game should not have been won yet", myGameBoard.checkIfWin());
		myGameBoard.placePiece(3, PieceType.RED);
		assertTrue("Game should have been won ", myGameBoard.checkIfWin());
		Point begin = myGameBoard.getWinBegin();
		Point end = myGameBoard.getWinEnd();
	}

	@Test
	void testMiddleRightSideHorizontalWin()
	{
		myGameBoard.placePiece(3, PieceType.RED);
		myGameBoard.placePiece(3, PieceType.RED);
		myGameBoard.placePiece(3, PieceType.BLACK);
		myGameBoard.placePiece(3, PieceType.RED);
		myGameBoard.placePiece(2, PieceType.BLACK);
		myGameBoard.placePiece(2, PieceType.RED);
		myGameBoard.placePiece(2, PieceType.RED);
		myGameBoard.placePiece(2, PieceType.RED);
		myGameBoard.placePiece(1, PieceType.RED);
		myGameBoard.placePiece(1, PieceType.BLACK);
		myGameBoard.placePiece(1, PieceType.BLACK);
		myGameBoard.placePiece(1, PieceType.RED);
		myGameBoard.placePiece(0, PieceType.BLACK);
		myGameBoard.placePiece(0, PieceType.BLACK);
		myGameBoard.placePiece(0, PieceType.BLACK);
		assertFalse("Game should not have been won yet", myGameBoard.checkIfWin());
		myGameBoard.placePiece(0, PieceType.RED);
		assertTrue("Game should have been won ", myGameBoard.checkIfWin());
		Point begin = myGameBoard.getWinBegin();
		Point end = myGameBoard.getWinEnd();
	}

	@Test
	void testMiddleLeftSideHorizontalWin()
	{
		myGameBoard.placePiece(3, PieceType.RED);
		myGameBoard.placePiece(3, PieceType.RED);
		myGameBoard.placePiece(3, PieceType.BLACK);
		myGameBoard.placePiece(3, PieceType.RED);
		myGameBoard.placePiece(4, PieceType.BLACK);
		myGameBoard.placePiece(4, PieceType.RED);
		myGameBoard.placePiece(4, PieceType.RED);
		myGameBoard.placePiece(4, PieceType.RED);
		myGameBoard.placePiece(5, PieceType.RED);
		myGameBoard.placePiece(5, PieceType.BLACK);
		myGameBoard.placePiece(5, PieceType.BLACK);
		myGameBoard.placePiece(5, PieceType.RED);
		myGameBoard.placePiece(6, PieceType.BLACK);
		myGameBoard.placePiece(6, PieceType.BLACK);
		myGameBoard.placePiece(6, PieceType.BLACK);
		assertFalse("Game should not have been won yet", myGameBoard.checkIfWin());
		myGameBoard.placePiece(6, PieceType.RED);
		assertTrue("Game should have been won ", myGameBoard.checkIfWin());
		Point begin = myGameBoard.getWinBegin();
		Point end = myGameBoard.getWinEnd();
	}
	
	@Test
	void testMiddleOfBoardWin()
	{
		myGameBoard.placePiece(3, PieceType.BLACK);
		myGameBoard.placePiece(3, PieceType.BLACK);
		myGameBoard.placePiece(3, PieceType.BLACK);
		myGameBoard.placePiece(3, PieceType.RED);
		myGameBoard.placePiece(4, PieceType.RED);
		myGameBoard.placePiece(4, PieceType.BLACK);
		myGameBoard.placePiece(4, PieceType.RED);
		myGameBoard.placePiece(4, PieceType.RED);
		myGameBoard.placePiece(5, PieceType.BLACK);
		myGameBoard.placePiece(5, PieceType.BLACK);
		myGameBoard.placePiece(5, PieceType.BLACK);
		myGameBoard.placePiece(5, PieceType.RED);
		myGameBoard.placePiece(2, PieceType.BLACK);
		myGameBoard.placePiece(2, PieceType.BLACK);
		myGameBoard.placePiece(2, PieceType.BLACK);
		assertFalse("Game should not have been won yet", myGameBoard.checkIfWin());
		myGameBoard.placePiece(2, PieceType.RED);
		assertTrue("Game should have been won ", myGameBoard.checkIfWin());
		Point begin = myGameBoard.getWinBegin();
		Point end = myGameBoard.getWinEnd();
	}

	@Test
	void testOutofBoundsWin() 
	{
		myGameBoard.placePiece(1, PieceType.RED);
		myGameBoard.placePiece(8, PieceType.BLACK);
		myGameBoard.placePiece(8, PieceType.BLACK);
		myGameBoard.placePiece(8, PieceType.BLACK);
		myGameBoard.placePiece(8, PieceType.BLACK);
		assertFalse("Game should not have been won, pieces out of bounds", myGameBoard.checkIfWin());
		
	}
	
	@Test
	void testIfColumnIsFull()
	{
		myGameBoard.placePiece(0, PieceType.RED);
		myGameBoard.placePiece(0, PieceType.RED);
		myGameBoard.placePiece(0, PieceType.RED);
		myGameBoard.placePiece(0, PieceType.RED);
		myGameBoard.placePiece(0, PieceType.RED);
		myGameBoard.placePiece(0, PieceType.RED);
		Point last = myGameBoard.getLastPoint();
		int x = last.x;
		assertTrue("Column is full", myGameBoard.isColumnFull(x));
	}
	
	@Test
	void testIfBoardIsFull()
	{
	
		for ( int i = 0; i < 6; i++)
		{
		myGameBoard.placePiece(0, PieceType.BLACK);
		myGameBoard.placePiece(1, PieceType.BLACK);
		myGameBoard.placePiece(2, PieceType.BLACK);
		myGameBoard.placePiece(3, PieceType.RED);
		myGameBoard.placePiece(4, PieceType.RED);
		myGameBoard.placePiece(5, PieceType.BLACK);
		myGameBoard.placePiece(6, PieceType.RED);
		}
		assertTrue("Game board is full", myGameBoard.isBoardFull());
		
	}
	
	@Test 
	void testBoardReset() 
	{
		assertTrue("Game Board is reset", myGameBoard.isBoardFull() == false);
		assertFalse("Game Board is not reset", myGameBoard.isBoardFull() == true);
	}

	
}