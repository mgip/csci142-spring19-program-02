package connectmodeltests;

/**
 * @purpose Test1x1Board tests the Connect Four Model
 * with the conditions of a 1x1 board.
 * @author williamcordarorios
 *
 * @datedue February 27, 2019, 11:59pm
 */

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import java.awt.Point;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import connectmodel.GameBoard;
import connectmodel.PieceType;

class Test1X1Board 
{
	
	private GameBoard myGameBoard;
	private PieceType[] myPieceTypes;

	@BeforeEach
	void setUp() throws Exception {
		myPieceTypes = new PieceType[2];
		myPieceTypes[0] = PieceType.RED;
		myPieceTypes[1] = PieceType.BLACK;
		myGameBoard = new GameBoard(1, 1, 1, myPieceTypes);
	}

	@Test
	void testPlacePiece()
	{
		myGameBoard.placePiece(0, PieceType.RED);
		Point last = myGameBoard.getLastPoint();
		int x = last.x;
		assertTrue("Piece was placed", x == 0);
	}
	
	@Test
	void testPieceOutOfBounds() 
	{
		myGameBoard.placePiece(2, PieceType.RED);
		Point last = myGameBoard.getLastPoint();
		int x = last.x;
		int y = last.y;
		assertFalse("Piece shouldn't be on board", x == 2);
		assertFalse("Piece shouldn't be on board", y == 0);
	}

	@Test
	void testOverlappingPieces() 
	{
		myGameBoard.placePiece(0, PieceType.RED);
		Point last = myGameBoard.getLastPoint();
		int x = last.x;
		assertTrue("Piece should be in column 0", x == 0);
		myGameBoard.placePiece(0, PieceType.BLACK);
		Point last2 = myGameBoard.getLastPoint();
		int y = last2.y;
		assertFalse("Piece should not be placed", y == 0);
	}
	
	@Test
	void testHorizontalLengthCount ()
	{
		myGameBoard.placePiece(0, PieceType.RED);
		Point last = myGameBoard.getLastPoint();
		int x = last.x;
		assertTrue("Horizontal length of last piece type is 1", x == 0);
	}
	
	@Test
	void testVerticalLengthCount ()
	{
		myGameBoard.placePiece(0, PieceType.RED);
		Point last = myGameBoard.getLastPoint();
		int x = last.x;
		assertTrue("Vertical length of last piece type is 1", x == 0);
	}
	
	@Test
	void testDiagonalLengthCount ()
	{
		myGameBoard.placePiece(0, PieceType.RED);
		Point last = myGameBoard.getLastPoint();
		int x = last.x;
		assertTrue("Diagonal length of last piece type is 1", x == 0);
	}
	
	@Test
	void testWin() 
	{
		myGameBoard.placePiece(0, PieceType.RED);
		assertTrue("Game should have been won ", myGameBoard.checkIfWin());
		myGameBoard.placePiece(0, PieceType.BLACK);
		assertFalse("Piece should not be placed",  myGameBoard.checkIfWin());
		Point begin = myGameBoard.getWinBegin();
		Point end = myGameBoard.getWinEnd();
	}
	
	@Test
	void testOutOfBoundsWin()
	{
		myGameBoard.placePiece(-1, PieceType.RED);
		assertFalse("Piece should not be placed",  myGameBoard.checkIfWin());
		myGameBoard.placePiece(2, PieceType.RED);
		assertFalse("Piece should not be placed",  myGameBoard.checkIfWin());
	}
	
	@Test
	void testBestMoveColumn()
	{
		myGameBoard.placePiece(0, PieceType.RED);
		assertTrue("Best Move already taken, game is won", myGameBoard.checkIfWin());
	}
	
	@Test
	void testIfColumnIsFull()
	{
		myGameBoard.placePiece(0, PieceType.RED);
		Point last = myGameBoard.getLastPoint();
		int x = last.x;
		assertTrue("Column is full", myGameBoard.isColumnFull(x));
	}
	
	@Test
	void testIfBoardIsFull()
	{
		
		myGameBoard.placePiece(0, PieceType.RED);		
		assertTrue("Game board is full", myGameBoard.isBoardFull());
	}
	
	@Test 
	void testBoardReset() 
	{
		assertTrue("Game Board is reset", myGameBoard.isBoardFull() == false);
		assertFalse("Game Board is not reset", myGameBoard.isBoardFull() == true);
	}
	
}
