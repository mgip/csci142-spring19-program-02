package connectmodeltests;

/**
 * @purpose TestPlayer tests the methods of the Player
 * class of the Connect Four Model.
 * @author madisongipson
 *
 * @datedue February 27, 2019, 11:59pm
 */

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import connectmodel.PieceType;
import connectmodel.Player;

class TestPlayer
{
	private String DEFAULT_NAME = "JohnCena";
	private Player myPlayer;
	private PieceType myPieceType;
	
	
	@BeforeEach
	void setUp() throws Exception 
	{
		myPlayer = new Player(DEFAULT_NAME, myPieceType);	
	}

	@Test
	void testInvalidNameSymbols()
	{
		Player player = new Player("JohnCena$$", myPieceType);
		assertFalse("Player name is not valid", DEFAULT_NAME.compareTo("JohnCena$$") == 0);
		DEFAULT_NAME = player.getName(); 
		assertTrue("Player name is now default name", DEFAULT_NAME.compareTo("JohnCena$$") == 0);
	}
	
	@Test
	void testInvalidNameSpace()
	{
		Player player = new Player("John Cena", myPieceType);
		assertFalse("Player name is not valid", DEFAULT_NAME.compareTo("John Cena") == 0);
		DEFAULT_NAME = player.getName(); 
		assertTrue("Player name is now default name", DEFAULT_NAME.compareTo("John Cena") == 0);
	}
	

}
