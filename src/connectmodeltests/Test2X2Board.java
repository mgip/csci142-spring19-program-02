package connectmodeltests;

/**
 * @purpose Test2x2Board tests the Connect Four Model
 * with the conditions of a 2x2 board.
 * @author williamcordarorios
 *
 * @datedue February 27, 2019, 11:59pm
 */


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import java.awt.Point;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import connectmodel.GameBoard;
import connectmodel.PieceType;

class Test2X2Board
{
	private GameBoard myGameBoard;
	private PieceType[] myPieceTypes;

	@BeforeEach
	void setUp() throws Exception {
		myPieceTypes = new PieceType[2];
		myPieceTypes[0] = PieceType.RED;
		myPieceTypes[1] = PieceType.BLACK;
		myGameBoard = new GameBoard(2, 2, 2, myPieceTypes);
	}

	@Test
	void testPlacePiece()
	{
		myGameBoard.placePiece(0, PieceType.RED);
		Point last = myGameBoard.getLastPoint();
		int x = last.x;
		assertTrue("Piece was placed", x == 0);
	}
	
	@Test
	void testPieceOutOfBounds() 
	{
		myGameBoard.placePiece(3, PieceType.RED);
		Point last = myGameBoard.getLastPoint();
		int x = last.x;
		int y = last.y;
		assertFalse("Piece shouldn't be on board", x == 3);
		assertFalse("Piece shouldn't be on board", y == 0);
		myGameBoard.placePiece(-1, PieceType.RED);
		Point last2 = myGameBoard.getLastPoint();
		int x2 = last2.x;
		int y2 = last2.y;
		assertFalse("Piece shouldn't be on board", x2 == 0);
		assertFalse("Piece shouldn't be on board", y2 == 0);
	}

	@Test
	void testOverlappingPieces() 
	{
		myGameBoard.placePiece(0, PieceType.RED);
		myGameBoard.placePiece(0, PieceType.RED);
		Point last = myGameBoard.getLastPoint();
		int x = last.x;
		int y = last.y;
		assertTrue("Piece should be in column 0", x == 0);
		assertTrue("Piece should be in row 1", y == 1);
		assertFalse("Piece is in the incorrect row", y == 0);
	}
	
	@Test
	void testHorizontalLengthCount ()
	{
		myGameBoard.placePiece(0, PieceType.RED);
		myGameBoard.placePiece(0, PieceType.BLACK);
		Point last = myGameBoard.getLastPoint();
		int x = last.x;
		assertTrue("Horizontal length of last piece type is 1", x == 0);
		myGameBoard.placePiece(1, PieceType.RED);
		Point last2 = myGameBoard.getLastPoint();
		int y = last2.y;
		assertTrue("Horizontal length of last piece type is 2", y == 1);
	}
	
	@Test
	void testVerticalLengthCount ()
	{
		myGameBoard.placePiece(0, PieceType.RED);
		myGameBoard.placePiece(1, PieceType.BLACK);
		Point last = myGameBoard.getLastPoint();
		int x = last.x;
		assertTrue("Vertical length of last piece type is 1", x == 0);
		myGameBoard.placePiece(0, PieceType.RED);
		Point last2 = myGameBoard.getLastPoint();
		int y = last2.y;
		assertTrue("Vertical length of last piece type is 2", y == 1);
	}
	
	@Test
	void testDiagonalLengthCount ()
	{
		myGameBoard.placePiece(0, PieceType.RED);
		myGameBoard.placePiece(1, PieceType.BLACK);
		Point last = myGameBoard.getLastPoint();
		int x = last.x;
		assertTrue("Diagonal length of last piece type is 1", x == 0);
		myGameBoard.placePiece(1, PieceType.RED);
		Point last2 = myGameBoard.getLastPoint();
		int y = last2.y;
		assertTrue("Diagonal length of last piece type is 2", y == 1);
	}
	
	@Test
	void testRowIndexCorrect() 
	{
		myGameBoard.placePiece(0, PieceType.RED);
		Point last = myGameBoard.getLastPoint();
		int x = last.x;
		int y = last.y;
		assertTrue("Piece should be in column 0", x == 0);
		assertTrue("Piece should be in row 1", y == 0);
		assertFalse("Rows seem inverted", y == 1);
	}
	
	@Test
	void testBottomRowWin() 
	{
		myGameBoard.placePiece(1, PieceType.RED);
		assertFalse("Game should not have been won yet", myGameBoard.checkIfWin());
		myGameBoard.placePiece(0, PieceType.RED);
		assertTrue("Game should have been won ", myGameBoard.checkIfWin());
		Point begin = myGameBoard.getWinBegin();
		Point end = myGameBoard.getWinEnd();
	}

	@Test
	void testLeftColumnWin() 
	{
		myGameBoard.placePiece(0, PieceType.RED);
		myGameBoard.placePiece(1, PieceType.BLACK);
		assertFalse("Game should not have been won yet", myGameBoard.checkIfWin());
		myGameBoard.placePiece(0, PieceType.RED);
		assertTrue("Game should have been won ", myGameBoard.checkIfWin());
		Point begin = myGameBoard.getWinBegin();
		Point end = myGameBoard.getWinEnd();
	}

	@Test
	void testRightColumnWin() 
	{
		myGameBoard.placePiece(0, PieceType.RED);
		myGameBoard.placePiece(1, PieceType.BLACK);
		assertFalse("Game should not have been won yet", myGameBoard.checkIfWin());
		myGameBoard.placePiece(1, PieceType.BLACK);
		assertTrue("Game should have been won ", myGameBoard.checkIfWin());
		Point begin = myGameBoard.getWinBegin();
		Point end = myGameBoard.getWinEnd();
	}

	@Test
	void testBottomRightDiagonalWin() 
	{
		myGameBoard.placePiece(0, PieceType.RED);
		myGameBoard.placePiece(1, PieceType.BLACK);
		assertFalse("Game should not have been won yet", myGameBoard.checkIfWin());
		myGameBoard.placePiece(1, PieceType.RED);
		assertTrue("Game should have been won ", myGameBoard.checkIfWin());
		Point begin = myGameBoard.getWinBegin();
		Point end = myGameBoard.getWinEnd();
	}
	
	@Test
	void testBottomLeftDiagonalWin() 
	{
		myGameBoard.placePiece(0, PieceType.RED);
		myGameBoard.placePiece(1, PieceType.BLACK);
		assertFalse("Game should not have been won yet", myGameBoard.checkIfWin());
		myGameBoard.placePiece(0, PieceType.BLACK);
		assertTrue("Game should have been won ", myGameBoard.checkIfWin());
		Point begin = myGameBoard.getWinBegin();
		Point end = myGameBoard.getWinEnd();
	}
	

	@Test
	void testOutofBoundsWin() 
	{
		myGameBoard.placePiece(1, PieceType.RED);
		myGameBoard.placePiece(3, PieceType.BLACK);
		myGameBoard.placePiece(3, PieceType.BLACK);
		assertFalse("Game should not have been won, pieces out of bounds", myGameBoard.checkIfWin());
		
	}
	
	@Test
	void testIfColumnIsFull()
	{
		myGameBoard.placePiece(0, PieceType.RED);
		myGameBoard.placePiece(0, PieceType.RED);
		Point last = myGameBoard.getLastPoint();
		int x = last.x;
		assertTrue("Column is full", myGameBoard.isColumnFull(x));
	}
	
	@Test
	void testIfBoardIsFull()
	{
		for ( int i = 0; i < 2; i++)
		{
		myGameBoard.placePiece(0, PieceType.RED);
		myGameBoard.placePiece(1, PieceType.BLACK);
		}
		assertTrue("Game board is full", myGameBoard.isBoardFull());
	}
	
	@Test 
	void testBoardReset() 
	{
		assertTrue("Game Board is reset", myGameBoard.isBoardFull() == false);
		assertFalse("Game Board is not reset", myGameBoard.isBoardFull() == true);
	}
	
	//TODO 
	@Test
	void testBestMoveColumn()
	{
		myGameBoard.placePiece(0, PieceType.RED);
		myGameBoard.placePiece(1, PieceType.BLACK);
		assertFalse("Game not won yet, should place piece in column 1 to win and block other player", myGameBoard.checkIfWin());
		myGameBoard.findBestMoveColumn(PieceType.RED);
		assertTrue("Successfully blocked other player & won game should have been won ", myGameBoard.checkIfWin());
		//For GameBoard, since there are many ways to pick the best next move in findBestMoveColumn(), 
		//it should only test the following:  if the next turn for placing a piece should lead to a win, 
		//it should pick that first.  If not a win, but placing a piece should block the other player, 
		//it should pick that move.  Otherwise, placing a piece should count the total number of horizontal, 
		//vertical and diagonal pieces up to "myWinLength" away from the piece to be placed and pick the maximum. 
	}
	
}