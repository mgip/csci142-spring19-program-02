package connectmodeltests;

/**
 * @purpose TestGameEngine tests the methods of the GameEngine
 * class of the Connect Four Model.
 * @author madisongipson
 *
 * @datedue February 27, 2019, 11:59pm
 */


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import java.awt.Point;
import java.util.Vector;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import connectmodel.GameBoard;
import connectmodel.GameEngine;
import connectmodel.Player;

class TestGameEngine {
	
	private GameEngine myGameEngine;
	private Player myPlayer;
	private GameBoard myGameBoard;
	private Vector<Player> myPlayers;

	@BeforeEach
	void setUp() throws Exception {
		myGameEngine = new GameEngine(myPlayer, myGameBoard);
	}

	@Test
	void test() {
		fail("Not yet implemented");
	}

	@Test
	void testSelectStartingPlayer()
	{
		myGameEngine.selectStartingPlayer(myPlayer);
		assertTrue("Starting player has been selected", 
				myGameEngine.selectStartingPlayer(myPlayer) == true);
		assertFalse("Starting player has been selected", 
				myGameEngine.selectStartingPlayer(myPlayer) == false);
	}
	
	@Test
	void testStartGame()
	{
		myGameEngine.startGame();
		assertTrue("Game has started", myGameEngine.startGame());
	}
	
	@Test
	void testSwitchPlayer()
	{
		Player player1 = myGameEngine.getPlayerUp();
		Player player2 = myGameEngine.switchPlayerUp();
		assertTrue("Player has switched", player1 != player2);
		assertFalse("Player hasn't switched", player1 == player2);
	}
	
	@Test
	void testPlacePiece()
	{
		myGameEngine.placePiece(0);
		Point last = myGameBoard.getLastPoint();
		int x = last.x;
		assertTrue("Piece was placed", x == 0);
	}
	
}
